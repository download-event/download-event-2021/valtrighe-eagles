<?php
    include( "functions.php" );
?>

<html lang = "en" >
    <head>
        <link rel = "stylesheet" href = "\css\style.css" >
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>
        <script src="\javascript\javascript.js"></script>
        <link rel="shortcut icon" href="#">
        <title>Home</title>
    </head>
    <script> clearInterval( 10000 ) w
</script>
    <body>
        <div class = "page">
            <img class="background_image" id="background_image_id" src="./images/img_<?php echo rand(1, 7); ?>.jpg" alt="Immagine qui">
            <div class = "contagius">
                <a>Country | Contagius | Death</a>
                <br>
                <?php print_covidContagius( 3, null ); ?>
            </div>
            <div class = "weather">
                <a>Weather</a>
                <br>
                <?php getWeather( 1, null ) ?>
            </div>
            <div class = "search-zone"> 
                <form class="search" action="search_city.php" method="get">
                    <input class="search-bar" id="search_bar" autocomplete="off" type="text" placeholder="Where are you headed?" name="location"></input>
                    <button class="search-button material-icons md-18" type="submit" ><p class="search">search</p></button>
                </form>
            </div>
        </div>
        <footer class = "footer">
            <p class="search material-icons md-18">copyright</p>
            <p class="search">2021 Valtrighe Eagles ( Non è molto ma è un lavoro onesto )</p>
        </footer>
    </body>
</html>