<?php

    function getWeather( $days, $city )
    {
        $weather_icons = [
            "Clear" => "<a class = 'material-icons' style='color:gray;' >cloud_off</a>",
            "Sunny" => "<a class = 'material-icons' style='color:yellow;' >brightness_high</a>",
            "Haze" => "<a class = 'material-icons' style='color:white;'  >dehaze</a>",
            "Cloudy" => "<a class = 'material-icons' style='color:gray;' >cloud</a>",
            "Partly Sunny" => "<a class = 'material-icons' style='color:yellow;' >brightness_medium</a>",
            "Partly Cloudy" => "<a class = 'material-icons' style='color:gray;' >cloud</a>",
            "Mostly Cloudy" => "<a class = 'material-icons' style='color:gray;' >cloud</a>",
            "Mostly Clear" => "<a class = 'material-icons' style='color:gray;' >cloud_off</a>",
            "Mostly Sunny" => "<a class = 'material-icons' style='color:yellow;' >light_mode</a>",
            "Rain Showers" => "<a class = 'material-icons' style='color:gray;' >storm</a>",
            "Light Rain" => "<a class = 'material-icons' style='color:blue;' >water_drop</a>",
            "Rain" => "<a class = 'material-icons' style='color:blue;' >water_drop</a>",
            "T-Storms" => "<a class = 'material-icons' style='color:gray;' >storm</a>"
        ];

        $json_file = file_get_contents("json/cities_state_full.json");
        $json_decoded = json_decode($json_file, true);
        $lines = count( $json_decoded );
        if ( $city == null )
        {
            for ( $i = 0; $i < 3; $i++ )
            {
                $random = rand( 0, $lines );
                $city = $json_decoded[$random]["city"];
                exec( "python weather.py $days $city", $output, $return_code );
                echo "<a class = 'onImage' >" . str_replace( "_" , " ", $city ) . " " . $output[0] . "° " . $weather_icons[$output[1]] . "</a>" . "<br>";
                $output = "";
            }
        }
        else
        {
            exec( "python weather.py $days $city", $output, $return_code );
            for ( $i = 0; $i < $days; $i++ )
            {
                echo "<a class = 'onImage' >" . date("d") + $i . "/" . "" . ucfirst( str_replace( "_" , " ", $city ) ) . " " . $output[ $i ] . "° " . $weather_icons[$output[ $i + $days ] ] . "</a>" . "<br>";
            }
        }
    }

    function print_covidContagius( $days, $city )
    {
        $json_file = file_get_contents("json/paesi_nomi.json");
        $json_decoded = json_decode($json_file, true);
        $lines = count( $json_decoded );
        if ( $city == null )
        {
            for ( $i = 0; $i < $days; $i++ )
            {
                $random = rand( 0, $lines );
                $yesterday = 3;
                $two_day_ago = 2;
                echo "<i class = 'material-icons' style='color:";if ( $yesterday > $two_day_ago ) { echo "red' >north"; } else { echo "green' >south"; } echo"</i>" . "<a class = 'onImage' id = 'state" . $random . "' >" . "cases_here" . "</a><br>";
                ?>
                <script>
                    $.getJSON("https://disease.sh/v3/covid-19/countries/" + "<?php echo $json_decoded[$random]["name"]; ?>" + "?yesterday=true", function( json_ )
                    {
                        document.getElementById("state" + <?php echo $random; ?>).innerHTML = "<?php echo $json_decoded[$random]["name"]; ?> | " + json_["todayCases"] + " | " + json_["todayDeaths"];
                    });
                </script>
            <?php
            }
        }
        else
        {
            $yesterday = 3;
            $two_day_ago = 2;
            echo "<i class = 'material-icons' style='color:";if ( $yesterday > $two_day_ago ) { echo "red' >north"; } else { echo "green' >south"; } echo"</i>" . "<a class = 'onImage' id = 'state_" . $city . "' >" . "cases_here" . "</a><br>";
        ?>
            <script>
                $.getJSON("https://disease.sh/v3/covid-19/countries/" + "<?php echo $city; ?>" + "?yesterday=true", function( json_ )
                {
                    document.getElementById("state" + <?php echo $city; ?>).innerHTML = "<?php echo $city; ?> | " + json_["todayCases"] + " | " + json_["todayDeaths"];
                });
            </script>
        <?php
        }
    }

    function news( $news_number, $city )
    {
        $json_file = file_get_contents("json/paesi_nomi.json");
        $json_decoded = json_decode($json_file, true);
        $lines = count( $json_decoded );
        $key = "b25ac0b4f3fc4a689028c72f394798cc";
        for ( $i = 0; $i < $news_number; $i++ )
        {
            echo "
            <div class = 'news_div'>
                <img class = 'aticle_image' id = 'article_image" . $i . "' src = '' alt = 'img' ><br>
                <a class = 'decoration article_title' id = 'article_title" . $i . "' >Title</a><br>
                <a class = 'decoration article_body' id = 'article_body" . $i . "' >Body</a><br>
                <a class = 'decoration article_author' id = 'article_author" . $i . "' >Author</a>
                <br><br>
            </div>";
        ?>
        <script>
            $.getJSON("https://newsapi.org/v2/everything?q=" + "<?php echo $city; ?>" + "&from=" + "<?php echo date("Y-m-d"); ?>" + "&sortBy=popularity&apiKey=" + "<?php echo $key ?>" , function( json )
            {
                document.getElementById( "article_image" + <?php echo $i; ?> ).src = json["articles"][<?php echo $i; ?>]["urlToImage"] ;
                document.getElementById( "article_title" + <?php echo $i; ?> ).innerHTML = "<a href = '" + json["articles"][<?php echo $i; ?>]["url"] + "' >" +  json["articles"][<?php echo $i; ?>]["title"] + "</a>" ;
                document.getElementById( "article_body" + <?php echo $i; ?> ).innerHTML = json["articles"][<?php echo $i; ?>]["description"];
                document.getElementById( "article_author" + <?php echo $i; ?> ).innerHTML = json["articles"][<?php echo $i; ?>]["author"];
                console.log(json);
            });
        </script>
        <?php
            }
        ?>
        <?php
    }

    function check_city( $city )
    {
    ?>
        <script>
            $.getJSON("https://countriesnow.space/api/v0.1/countries", function( json )
            {
                console.log( json );
            });
        </script>
    <?php
    }
?>
