<?php

session_start();

include( "functions.php" );

if ( isset( $_GET["location"] ) )
{
    $location = $_GET["location"];
    $headers = @get_headers('https://www.weather-forecast.com/locations/'. $location . '/forecasts/latest');
    if ( $headers[0] != "HTTP/1.1 404 Not Found" )
    { ?>
        <html>
            <head>
                <link rel = "stylesheet" href = "\css\style_css_second_page.css">
                <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
                <script src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>
                <title>Second Page</title>
            </head>
            <body>
                <div class = "search-zone"> 
                    <form class="search" action="search_city.php" method="get">
                        <input class="search-bar" id="search_bar" autocomplete="off" type="text" placeholder="Where are you headed?" name="location">
                        <button class="search-button material-icons md-18" type="submit" ><p class="search">search</p></button>
                    </form>
                </div>
                <div class = "page">
                    <div class = "contagius" >
                        <?php print_covidContagius( 1, $location ); ?>
                    </div>
                    <div class = "news" >
                        <?php news( 4, $location ); ?>
                    </div>
                    <div class = "weather decoration" >
                        <?php getWeather( 6, $location ); ?>
                    </div>
                </div> 
            </body>
        </html>
    <?php }
    else
    {
        header( "Location: index.php?location=invalid" );
    }
}
?>
