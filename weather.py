import asyncio
import time
import sys
import python_weather

weathers = []

async def get_weather():
    client = python_weather.Client( format=python_weather.IMPERIAL )
    weather = await client.find( sys.argv[2].replace( "_", " " ) )
    weathers.append( weather.current )
    counter = 0
    for forecast in weather.forecasts:
        if counter == sys.argv[1]:
            break
        weathers.append( forecast )
        counter += 1
    await client.close()


loop = asyncio.get_event_loop()
loop.run_until_complete( get_weather() )

for i in range(  int( sys.argv[1] ) ):
    print( ( int( ( weathers[i].temperature - 32 ) * 5 / 9 ) ) )

for i in range(  int( sys.argv[1] ) ):
    print( weathers[i].sky_text )
